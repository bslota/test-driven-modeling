package com.bartslota.c4;

import java.util.function.Function;

import com.structurizr.Workspace;
import com.structurizr.model.Container;
import com.structurizr.model.SoftwareSystem;
import com.structurizr.view.StaticView;
import com.structurizr.view.ViewSet;

import static com.bartslota.c4.Protocols.KAFKA;
import static com.bartslota.c4.Protocols.REST;
import static com.bartslota.c4.Protocols.SOAP;
import static com.bartslota.c4.ViewCreator.setupView;

class ContainersDiagram {

    static InternalContainers create(Workspace workspace, SoftwareSystem internalSystem, ExternalSystems externalSystems, Actors actors) {
        InternalContainers internalContainers = new InternalContainers(internalSystem);
        internalContainers.createUsages(externalSystems, actors);
        setupContainerView(workspace, internalSystem);
        return internalContainers;
    }

    private static void setupContainerView(Workspace workspace, SoftwareSystem microservices) {
        Function<ViewSet, StaticView> containerViewCreator = views ->
                views.createContainerView(microservices,
                        "E-bike microservices",
                        "E-bike microservices view");
        setupView(workspace, containerViewCreator);
    }
}

class InternalContainers {

    private static final String SPRING_BOOT_MICROSERVICE = "Spring boot microservice";
    private static final String REACT = "ReactJS application";
    final Container clientsAndUsers;
    final Container catalogue;
    final Container payments;
    final Container reservationsAndLendings;
    final Container availabilityManagement;
    final Container priceConfiguration;
    final Container stationCommunicator;
    final Container rangeCalculator;
    final Container frontend;

    InternalContainers(SoftwareSystem internalSystem) {
        clientsAndUsers = internalSystem.addContainer("Clients and users", "Managing clients and their accounts", SPRING_BOOT_MICROSERVICE);
        catalogue = internalSystem.addContainer("Catalogue", "Bike, and station catalogue", SPRING_BOOT_MICROSERVICE);
        payments = internalSystem.addContainer("Payments", "Facade to online payments", SPRING_BOOT_MICROSERVICE);
        reservationsAndLendings = internalSystem.addContainer("Reservations and lendings", "Handling reservations and lendings", SPRING_BOOT_MICROSERVICE);
        availabilityManagement = internalSystem.addContainer("Availability management", "Handling asset availability", SPRING_BOOT_MICROSERVICE);
        priceConfiguration = internalSystem.addContainer("Price configuration", "Configuring price catalogue", SPRING_BOOT_MICROSERVICE);
        stationCommunicator = internalSystem.addContainer("Station communicator", "Communicates with stations", SPRING_BOOT_MICROSERVICE);
        rangeCalculator = internalSystem.addContainer("Range calculator", "Estimates range of a vehicle", SPRING_BOOT_MICROSERVICE);
        frontend = internalSystem.addContainer("Frontend app", "UI for operational and business usage", REACT);
    }

    public void createUsages(ExternalSystems externalSystems, Actors actors) {
        //catalogue
        catalogue.uses(availabilityManagement, "Register new assets", KAFKA);

        //payments
        payments.uses(externalSystems.onlinePayments, "forwards payment requests", REST);
        payments.uses(priceConfiguration, "Listens to price-defining events", KAFKA);
        payments.uses(clientsAndUsers, "Listens to client-defining events", KAFKA);

        //station communicator
        stationCommunicator.uses(externalSystems.station, "send vehicle registrations and locks", SOAP);
        stationCommunicator.uses(availabilityManagement, "Lock/unlock assets", KAFKA);

        //station
        externalSystems.station.uses(stationCommunicator, "send station-related, and vehicle-related events", SOAP);

        //reservations
        reservationsAndLendings.uses(clientsAndUsers, "Listens to user-defining events", KAFKA);
        reservationsAndLendings.uses(availabilityManagement, "Send asset lock/unlock request", REST);
        reservationsAndLendings.uses(payments, "send payment requests", REST);
        reservationsAndLendings.uses(stationCommunicator, "send vehicle lock requests", REST);
        reservationsAndLendings.uses(stationCommunicator, "Listens to vehicle-related events", KAFKA);

        //range calculation
        rangeCalculator.uses(stationCommunicator, "Listens to vehicle-related events", KAFKA);

        //frontend
        frontend.uses(catalogue, "Get/set bikes, stations", REST);
        frontend.uses(priceConfiguration, "Get/set prices", REST);
        frontend.uses(clientsAndUsers, "Get/set clients, and users", REST);
        frontend.uses(reservationsAndLendings, "Create/read reservations", REST);
        frontend.uses(rangeCalculator, "Get current range of bikes", REST);
        actors.customer.uses(frontend, "uses");
        actors.employee.uses(frontend, "uses");
    }

}
