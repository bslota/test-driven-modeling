package com.bartslota.c4;

import java.util.List;
import java.util.stream.Stream;

import com.bartslota.c4.styles.StyleProvider;
import com.structurizr.model.Container;

import static com.bartslota.c4.TeamStyle.teamStyle;
import static java.util.stream.Collectors.toList;

enum Teams {
    TEAM_ALFA(teamStyle("#3AAB70", "#000000")),
    TEAM_BETA(teamStyle("#FFAF30", "#000000")),
    TEAM_GAMMA(teamStyle("#00ADD5", "#000000")),
    SHARED(teamStyle("#C36359", "#000000"));

    private final TeamStyle teamStyle;

    Teams(TeamStyle teamStyle) {
        this.teamStyle = teamStyle;
    }

    public void assignTo(Container container) {
        container.addTags(name());
    }

    public static List<StyleProvider> styleProviders() {
        return Stream.of(values())
                     .map(Teams::teamsStyleProvider)
                     .collect(toList());
    }

    private StyleProvider teamsStyleProvider() {
        return styles -> styles.addElementStyle(name())
                               .background(teamStyle.backgroundColorHex())
                               .color(teamStyle.fontColorHex());
    }
}