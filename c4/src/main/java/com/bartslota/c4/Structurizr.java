package com.bartslota.c4;

import java.io.InputStream;
import java.util.Properties;

import com.bartslota.c4.styles.DiagramStyles;
import com.structurizr.Workspace;
import com.structurizr.api.StructurizrClient;
import com.structurizr.model.Model;
import com.structurizr.model.SoftwareSystem;
import com.structurizr.model.Tags;
import com.structurizr.view.Shape;
import com.structurizr.view.Styles;

import static java.lang.Long.parseLong;

public class Structurizr {

    public static void main(String[] args) throws Exception {
        createWorkspace();
    }

    private static void createWorkspace() throws Exception {
        //workspace
        Workspace workspace = new Workspace("E-bike architecture", "E-bike system architecture");

        //model
        Model model = workspace.getModel();

        //internal system
        SoftwareSystem internalSystem = model.addSoftwareSystem("E-bike", "E-bike microservices");

        //actors
        Actors actors = Actors.createFor(workspace);
        actors.customer.uses(internalSystem, "uses");
        actors.employee.uses(internalSystem, "uses");

        //external systems
        ExternalSystems externalSystems = ContextDiagram.create(workspace, model, internalSystem);

        //containers
        InternalContainers internalContainers = ContainersDiagram.create(workspace, internalSystem, externalSystems, actors);

        //team responsibilities
        TeamResponsibilities.setupTeamResponsibilities(internalContainers);
        TerraIncognita.tag(internalContainers);

        //styling
        applyStyles(workspace);

        //export
        uploadWorkspaceToStructurizr(workspace);
    }

    private static void uploadWorkspaceToStructurizr(Workspace workspace) throws Exception {
        try (InputStream inputStream = Structurizr.class.getClassLoader()
                                                        .getResourceAsStream("secret.properties")) {
            Properties prop = new Properties();
            prop.load(inputStream);
            long workspaceId = parseLong(prop.getProperty("workspace.id"));
            String apiKey = prop.getProperty("api.key");
            String apiSecret = prop.getProperty("api.secret");
            String url = prop.getProperty("workspace.url");
            StructurizrClient structurizrClient = new StructurizrClient(url, apiKey, apiSecret);
            structurizrClient.putWorkspace(workspaceId, workspace);
        }
    }

    private static void applyStyles(Workspace workspace) {
        Styles styles = workspace.getViews().getConfiguration().getStyles();
        styles.addElementStyle(Tags.SOFTWARE_SYSTEM).background("#1168bd").color("#ffffff");
        styles.addElementStyle(CustomTags.EXTERNAL_SYSTEM).background("#d5d5d5").color("#000000");
        styles.addElementStyle(Tags.PERSON).background("#08427b").color("#ffffff").shape(Shape.Person);

        final DiagramStyles diagramStyles = new DiagramStyles(workspace);
        diagramStyles.applyStyles(Teams.styleProviders());
        diagramStyles.applyStyle(TerraIncognita.styleProvider());
    }

}

