package com.bartslota.c4;

import com.bartslota.c4.styles.StyleProvider;
import com.structurizr.model.Container;

class TerraIncognita {

    private static final String TAG = "TERRA_INCOGNITA";
    private static final int OPACITY_PERCENTAGE = 20;

    static void tag(InternalContainers containers) {
        //no unassigned responsibilities nat the moment
    }

    static StyleProvider styleProvider() {
        return styles -> styles.addElementStyle(TAG).opacity(OPACITY_PERCENTAGE);
    }

    private static void tag(Container container) {
        container.addTags(TAG);
    }
}
