package com.bartslota.c4;

interface Protocols {

    String REST = "HTTP REST";
    String SOAP = "SOAP";
    String KAFKA = "Kafka";
}
