package com.bartslota.c4.styles;

import com.structurizr.view.ElementStyle;
import com.structurizr.view.Styles;

import java.util.function.Function;

public interface StyleProvider extends Function<Styles, ElementStyle> {

}
