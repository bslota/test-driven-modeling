package com.bartslota.reservations;

/**
 * @author bslota on 05/01/2022
 */
record ReservationDto(String vehicleId) {
}
