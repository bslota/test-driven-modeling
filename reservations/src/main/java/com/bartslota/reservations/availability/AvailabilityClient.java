package com.bartslota.reservations.availability;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "availability", url = "${availability.api.url}", path = "${availability.api.root-path}")
interface AvailabilityClient {

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<?> send(@RequestBody Command command, @RequestHeader(AUTHORIZATION) String authorization);
}
