package com.bartslota.reservations.availability;

/**
 * @author bslota on 05/01/2022
 */
public record LockCommand(String assetId, Integer durationInMinutes) implements Command {

    private static final String TYPE = "LOCK";

    @Override
    public String getType() {
        return TYPE;
    }

}
