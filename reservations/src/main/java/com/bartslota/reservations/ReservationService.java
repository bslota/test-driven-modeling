package com.bartslota.reservations;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bartslota.reservations.availability.AvailabilityFacade;
import com.bartslota.reservations.availability.LockCommand;

/**
 * @author bslota on 05/01/2022
 */
@Service
class ReservationService {

    private static final Integer DEFAULT_LOCK_DURATION_IN_MINUTES = 10;

    private final AvailabilityFacade availabilityFacade;

    ReservationService(AvailabilityFacade availabilityFacade) {
        this.availabilityFacade = availabilityFacade;
    }

    ResponseEntity<?> makeReservationFor(String vehicleId, String auth) {
        return availabilityFacade.send(new LockCommand(vehicleId, DEFAULT_LOCK_DURATION_IN_MINUTES), auth);
    }
}
