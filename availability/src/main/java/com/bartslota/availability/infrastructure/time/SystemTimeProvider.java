package com.bartslota.availability.infrastructure.time;

import java.time.InstantSource;

import org.springframework.stereotype.Component;

import com.bartslota.availability.domain.TimeProvider;

/**
 * @author bslota on 25/03/2022
 */
@Component
class SystemTimeProvider implements TimeProvider {

    @Override
    public InstantSource get() {
        return InstantSource.system();
    }
}
