package com.bartslota.availability.infrastructure.rest;

import java.security.Principal;
import java.time.Duration;
import java.util.function.Function;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bartslota.availability.application.AvailabilityService;
import com.bartslota.availability.commands.Activate;
import com.bartslota.availability.commands.Command;
import com.bartslota.availability.commands.Lock;
import com.bartslota.availability.commands.LockIndefinitely;
import com.bartslota.availability.commands.Register;
import com.bartslota.availability.commands.Unlock;
import com.bartslota.availability.commands.Withdraw;
import com.bartslota.availability.domain.AssetId;
import com.bartslota.availability.domain.OwnerId;
import com.bartslota.availability.events.DomainEvent;

import io.vavr.control.Either;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author bslota on 26/03/2022
 */
@RestController
@RequestMapping(path = "/api/assets/commands", consumes = APPLICATION_JSON_VALUE)
class AssetAvailabilityController {

    private final AvailabilityService availabilityService;

    AssetAvailabilityController(AvailabilityService availabilityService) {
        this.availabilityService = availabilityService;
    }

    @PostMapping
    ResponseEntity<?> handle(@RequestBody Command command, Principal user) {
        return switch (command) {
            case Register c -> handle(c);
            case Withdraw c -> handle(c);
            case Activate c -> handle(c);
            case Lock c -> handle(c, user);
            case LockIndefinitely c -> handle(c, user);
            case Unlock c -> handle(c, user);
        };
    }

    private ResponseEntity<?> handle(Register command) {
        return execute(cmd -> availabilityService.registerAssetWith(AssetId.of(command.assetId())), command);
    }

    private ResponseEntity<?> handle(Withdraw command) {
        return execute(cmd -> availabilityService.withdraw(AssetId.of(command.assetId())), command);
    }

    private ResponseEntity<?> handle(Activate command) {
        return execute(cmd -> availabilityService.activate(AssetId.of(command.assetId())), command);
    }

    private ResponseEntity<?> handle(Lock command, Principal user) {
        return execute(cmd -> availabilityService.lock(AssetId.of(command.assetId()), OwnerId.of(user.getName()), Duration.ofMinutes(command.durationInMinutes())), command);
    }

    private ResponseEntity<?> handle(LockIndefinitely command, Principal user) {
        return execute(cmd -> availabilityService.lockIndefinitely(AssetId.of(command.assetId()), OwnerId.of(user.getName())), command);
    }

    private ResponseEntity<?> handle(Unlock command, Principal user) {
        return execute(cmd -> availabilityService.unlock(AssetId.of(command.assetId()), OwnerId.of(user.getName())), command);
    }

    private <T extends Command> ResponseEntity<?> execute(Function<T, Either<? extends DomainEvent, ? extends DomainEvent>> function, T command) {
        return function
                .apply(command)
                .fold(rejected -> ResponseEntity.unprocessableEntity().build(),
                        accepted -> ResponseEntity.status(ACCEPTED).build());
    }

}
