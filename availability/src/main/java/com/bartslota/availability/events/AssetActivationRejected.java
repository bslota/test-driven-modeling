package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetActivationRejected extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetActivationRejected";

    private final String assetId;
    private final String reason;

    @JsonCreator
    private AssetActivationRejected(String assetId, String reason, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
        this.reason = reason;
    }

    public static AssetActivationRejected dueToMissingAssetWith(String assetId, Instant occurrenceTime) {
        return new AssetActivationRejected(assetId, "ASSET_IS_MISSING", occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    String getReason() {
        return reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetActivationRejected)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetActivationRejected that = (AssetActivationRejected) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId, reason);
    }
}
