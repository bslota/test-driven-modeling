package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetLockExpired extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetLockExpired";

    private final String assetId;

    @JsonCreator
    private AssetLockExpired(String assetId, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
    }

    public static AssetLockExpired from(String assetId, Instant occurrenceTime) {
        return new AssetLockExpired(assetId, occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetLockExpired)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetLockExpired that = (AssetLockExpired) o;
        return Objects.equals(assetId, that.assetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId);
    }
}
