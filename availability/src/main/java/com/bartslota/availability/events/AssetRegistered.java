package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetRegistered extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetRegistered";

    private final String assetId;

    @JsonCreator
    private AssetRegistered(String assetId, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
    }

    public static AssetRegistered from(String assetId, Instant instant) {
        return new AssetRegistered(assetId, instant);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetRegistered)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetRegistered that = (AssetRegistered) o;
        return Objects.equals(assetId, that.assetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId);
    }
}
