package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetUnlockingRejected extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetUnlockingRejected";

    private final String assetId;
    private final String ownerId;
    private final String reason;

    @JsonCreator
    private AssetUnlockingRejected(String assetId, String ownerId, String reason, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
        this.ownerId = ownerId;
        this.reason = reason;
    }

    public static AssetUnlockingRejected dueToMissingLockOnTheAssetWith(String assetId, String ownerId, Instant occurrenceTime) {
        return new AssetUnlockingRejected(assetId, ownerId, "NO_LOCK_ON_THE_ASSET", occurrenceTime);
    }

    public static AssetUnlockingRejected dueToMissingAssetWith(String assetId, String ownerId, Instant occurrenceTime) {
        return new AssetUnlockingRejected(assetId, ownerId, "ASSET_IS_MISSING", occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    String getOwnerId() {
        return ownerId;
    }

    String getReason() {
        return reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetUnlockingRejected)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetUnlockingRejected that = (AssetUnlockingRejected) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(ownerId, that.ownerId) && Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId, ownerId, reason);
    }
}
