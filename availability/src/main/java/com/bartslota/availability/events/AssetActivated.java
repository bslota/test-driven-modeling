package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.bartslota.availability.domain.AssetId;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetActivated extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetActivated";

    private final String assetId;

    @JsonCreator
    private AssetActivated(String assetId, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
    }

    public static AssetActivated from(String assetId, Instant occurrenceTime) {
        return new AssetActivated(assetId, occurrenceTime);
    }

    String getAssetId() {
        return assetId;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetActivated)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetActivated that = (AssetActivated) o;
        return Objects.equals(assetId, that.assetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId);
    }
}
