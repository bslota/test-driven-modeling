package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetRegistrationRejected extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetRegistrationRejected";

    private final String assetId;
    private final String reason;

    @JsonCreator
    private AssetRegistrationRejected(String assetId, String reason, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
        this.reason = reason;
    }

    public static AssetRegistrationRejected dueToAlreadyExistingAssetWith(String assetId, Instant occurrenceTime) {
        return new AssetRegistrationRejected(assetId, "ASSET_ALREADY_EXISTS", occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    String getReason() {
        return reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetRegistrationRejected)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetRegistrationRejected that = (AssetRegistrationRejected) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId, reason);
    }
}

