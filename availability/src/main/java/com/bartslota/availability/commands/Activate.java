package com.bartslota.availability.commands;

/**
 * @author bslota on 25/11/2021
 */
public record Activate(String assetId) implements Command {

    static final String TYPE = "ACTIVATE";

    @Override
    public String getType() {
        return TYPE;
    }
}
