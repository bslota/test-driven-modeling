package com.bartslota.availability.commands;

/**
 * @author bslota on 25/11/2021
 */
public record Unlock(String assetId) implements Command {

    static final String TYPE = "UNLOCK";

    @Override
    public String getType() {
        return TYPE;
    }
}
