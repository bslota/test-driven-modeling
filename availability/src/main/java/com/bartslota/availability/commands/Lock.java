package com.bartslota.availability.commands;

/**
 * @author bslota on 25/11/2021
 */
public record Lock(String assetId, Integer durationInMinutes) implements Command {

    static final String TYPE = "LOCK";

    @Override
    public String getType() {
        return TYPE;
    }
}
