package com.bartslota.availability.commands;

/**
 * @author bslota on 25/11/2021
 */
public record Register(String assetId) implements Command {

    static final String TYPE = "REGISTER";

    @Override
    public String getType() {
        return TYPE;
    }
}
