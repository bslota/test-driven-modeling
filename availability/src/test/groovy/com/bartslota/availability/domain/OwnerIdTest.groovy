package com.bartslota.availability.domain

import spock.lang.Specification

import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerIdValue

/**
 * @author bslota on 19/03/2022
 */
class OwnerIdTest extends Specification {

    def "two owner ids should be equal when created from the same value"() {
        given:
            String someValue = someOwnerIdValue()

        expect:
            OwnerId.of(someValue) == OwnerId.of(someValue)
    }

    def "two owner ids should not be equal when created from the different values"() {
        expect:
            OwnerId.of(someOwnerIdValue()) != OwnerId.of(someOwnerIdValue())
    }

}
