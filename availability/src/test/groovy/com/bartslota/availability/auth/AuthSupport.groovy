package com.bartslota.availability.auth

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder

/**
 * @author bslota on 09/01/2022
 */
trait AuthSupport {

    void authorizedAsCustomer() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("ADAM_123", "pass_123", [new SimpleGrantedAuthority("ROLE_CUSTOMER")]))
    }

    void authorizedAsAdmin() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("ADMIN_123", "admin_pass_123", [new SimpleGrantedAuthority("ROLE_ADMIN")]))
    }

}