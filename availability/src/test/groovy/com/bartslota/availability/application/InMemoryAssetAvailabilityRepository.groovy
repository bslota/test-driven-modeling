package com.bartslota.availability.application

import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.AssetId

import java.util.stream.Stream

/**
 * @author bslota on 23/11/2021
 */
class InMemoryAssetAvailabilityRepository implements AssetAvailabilityRepository {

    Map<AssetId, AssetAvailability> content = [:]

    @Override
    void save(AssetAvailability assetAvailability) {
        content.put(assetAvailability.id() , assetAvailability)
    }

    @Override
    Optional<AssetAvailability> findBy(AssetId assetId) {
        Optional<AssetAvailability> asset = Optional.ofNullable(content[assetId])
        content.remove(assetId)
        asset
    }

    @Override
    Stream<AssetAvailability> findOverdue() {
        null
    }
}
