package com.bartslota.availability.application

import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.events.AssetActivated
import com.bartslota.availability.events.AssetLockExpired
import com.bartslota.availability.events.AssetLocked
import com.bartslota.availability.events.AssetLockedIndefinitely
import com.bartslota.availability.events.AssetRegistered
import com.bartslota.availability.events.AssetUnlocked
import com.bartslota.availability.events.AssetWithdrawn
import com.bartslota.availability.events.DomainEvent
import com.bartslota.availability.events.DomainEventsPublisher
import groovy.transform.SelfType

import java.time.Duration
import java.time.Instant

/**
 * @author bslota on 25/11/2021
 */
@SelfType(TimeSupport)
trait AssetAvailabilityEventsSupport {

    DomainEventsPublisher publisher = new InMemoryDomainEventPublisher()

    boolean assetRegisteredEventWasPublishedFor(AssetId assetId) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetRegistered.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetRegisteredEventWasNotPublishedFor(AssetId assetId) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetRegistered.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetWithdrawnEventWasPublishedFor(AssetId assetId) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetWithdrawn.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetWithdrawnEventWasNotPublishedFor(AssetId assetId) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetWithdrawn.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetActivatedEventWasPublishedFor(AssetId assetId) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetActivated.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetActivatedEventWasNotPublishedFor(AssetId assetId) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetActivated.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetLockedEventWasPublishedFor(AssetId assetId, OwnerId ownerId, Duration duration) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetLocked.from(assetId.asString(), ownerId.asString(), timeProvider().instant() + duration, timeProvider().instant()) }
    }

    boolean assetLockedEventWasNotPublishedFor(AssetId assetId, OwnerId ownerId, Duration duration) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetLocked.from(assetId.asString(), ownerId.asString(), timeProvider().instant() + duration, timeProvider().instant()) }
    }

    boolean assetLockedIndefinitelyEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetLockedIndefinitely.from(assetId.asString(), ownerId.asString(), timeProvider().instant()) }
    }

    boolean assetLockedIndefinitelyEventWasNotPublishedFor(AssetId assetId, OwnerId ownerId) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetLockedIndefinitely.from(assetId.asString(), ownerId.asString(), timeProvider().instant()) }
    }

    boolean assetUnlockedEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetUnlocked.from(assetId.asString(), ownerId.asString(), timeProvider().instant()) }
    }

    boolean assetUnlockedEventWasNotPublishedFor(AssetId assetId, OwnerId ownerId) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetUnlocked.from(assetId.asString(), ownerId.asString(), timeProvider().instant()) }
    }

    boolean assetLockExpiredEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        publisher.thereWasAnEventFulfilling { DomainEvent e -> e == AssetLockExpired.from(assetId.asString(), timeProvider().instant()) }
    }

    boolean assetLockExpiredEventWasNotPublishedFor(AssetId assetId, OwnerId ownerId) {
        publisher.thereWasNoEventFulfilling { DomainEvent e -> e == AssetLockExpired.from(assetId.asString(), timeProvider().instant()) }
    }
}