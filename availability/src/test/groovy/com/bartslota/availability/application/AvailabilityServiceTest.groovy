package com.bartslota.availability.application

import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.domain.TimeProvider
import com.bartslota.availability.domain.TimeProviderFixture
import spock.lang.Specification

import java.time.Duration
import java.time.Instant

import static com.bartslota.availability.domain.AssetIdFixture.someAssetId
import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId
import static java.time.Instant.now

/**
 * @author bslota on 23/11/2021
 */
class AvailabilityServiceTest extends Specification implements AssetAvailabilityEventsSupport, AssetAvailabilityStoreSupport, TimeSupport {

    private AssetAvailabilityRepository repository = new InMemoryAssetAvailabilityRepository()
    private TimeProvider timeProvider = TimeProviderFixture.timeProvider().thatIsFixedFor(now())
    private AvailabilityService availabilityService = new AvailabilityService(repository, publisher, timeProvider)

    def "should register asset"() {
        given:
            AssetId assetId = someAssetId()

        when:
            availabilityService.registerAssetWith(assetId)

        then:
            assetIsRegisteredWith(assetId)
    }

    def "should emit AssetRegistered event when successfully registered asset"() {
        given:
            AssetId assetId = someAssetId()

        when:
            availabilityService.registerAssetWith(assetId)

        then:
            assetRegisteredEventWasPublishedFor(assetId)
    }

    def "should not register asset when one with the same id already exists"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            availabilityService.registerAssetWith(asset.id())

        then:
            assetRegisteredEventWasNotPublishedFor(asset.id())
    }

    def "should activate asset"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            availabilityService.activate(asset.id())

        then:
            assetIsActivatedWith(asset.id())
    }

    def "should emit AssetActivated event when successfully activated asset"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            availabilityService.activate(asset.id())

        then:
            assetActivatedEventWasPublishedFor(asset.id())
    }

    def "should not emit AssetActivated event when activation is not possible due to asset not being registered"() {
        given:
            AssetId idOfNotRegisteredAsset = someAssetId()

        when:
            availabilityService.activate(idOfNotRegisteredAsset)

        then:
            assetActivatedEventWasNotPublishedFor(idOfNotRegisteredAsset)
    }

    def "should lock activated asset"() {
        given:
            AssetAvailability asset = activatedAsset()

        and:
            OwnerId ownerId = someOwnerId()

        when:
            availabilityService.lock(asset.id(), ownerId, someValidDuration())

        then:
            thereIsALockedAssetWith(asset.id(), ownerId)
    }

    def "should emit AssetLocked event when asset is successfully locked"() {
        given:
            AssetAvailability asset = activatedAsset()

        and:
            OwnerId ownerId = someOwnerId()
            Duration duration = someValidDuration()

        when:
            availabilityService.lock(asset.id(), ownerId, duration)

        then:
            assetLockedEventWasPublishedFor(asset.id(), ownerId, duration)
    }

    def "should not emit AssetLocked event when asset is not registered"() {
        given:
            AssetId idOfNotExistingAsset = someAssetId()
            OwnerId ownerId = someOwnerId()
            Duration duration = someValidDuration()

        when:
            availabilityService.lock(idOfNotExistingAsset, ownerId, duration)

        then:
            assetLockedEventWasNotPublishedFor(idOfNotExistingAsset, ownerId, duration)
    }

    def "should lock indefinitely already locked asset"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        when:
            availabilityService.lockIndefinitely(asset.id(), ownerId)

        then:
            thereIsAnIndefinitelyLockedAssetWith(asset.id(), ownerId)
    }

    def "should emit AssetLockedIndefinitely event when asset is successfully locked indefinitely"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        when:
            availabilityService.lockIndefinitely(asset.id(), ownerId)

        then:
            assetLockedIndefinitelyEventWasPublishedFor(asset.id(), ownerId)
    }

    def "should not emit AssetLockedIndefinitely event when asset is not registered"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetId idOfNotExistingAsset = someAssetId()

        when:
            availabilityService.lockIndefinitely(idOfNotExistingAsset, ownerId)

        then:
            assetLockedIndefinitelyEventWasNotPublishedFor(idOfNotExistingAsset, ownerId)
    }

    def "should unlock asset"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        when:
            availabilityService.unlock(asset.id(), ownerId)

        then:
            assetIsActivatedWith(asset.id())
    }

    def "should emit AssetUnlocked event when asset is successfully unlocked"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        when:
            availabilityService.unlock(asset.id(), ownerId)

        then:
            assetUnlockedEventWasPublishedFor(asset.id(), ownerId)
    }

    def "should not emit AssetUnlocked event when asset is not registered"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetId idOfNotExistingAsset = someAssetId()

        when:
            availabilityService.unlock(idOfNotExistingAsset, ownerId)

        then:
            assetUnlockedEventWasNotPublishedFor(idOfNotExistingAsset, ownerId)
    }

    def "should unlock asset when its lock is overdue"() {
        given:
            AssetAvailability asset = assetWithOverdueLock()

        when:
            availabilityService.unlockIfOverdue(asset)

        then:
            assetIsActivatedWith(asset.id())
    }

    def "should emit AssetUnlocked event when asset with overdue lock is successfully unlocked"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetWithOverdueLock(ownerId)

        when:
            availabilityService.unlockIfOverdue(asset)

        then:
            assetLockExpiredEventWasPublishedFor(asset.id(), ownerId)
    }

    def "should withdraw existing asset"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            availabilityService.withdraw(asset.id())

        then:
            thereIsAWithdrawnAssetWith(asset.id())
    }

    def "should emit AssetWithdrawn event when asset is successfully withdrawn"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            availabilityService.withdraw(asset.id())

        then:
            assetWithdrawnEventWasPublishedFor(asset.id())
    }

    def "should not emit AssetWithdrawn event when withdrawal is not possible due to asset not being registered"() {
        given:
            AssetId idOfNotRegisteredAsset = someAssetId()

        when:
            availabilityService.withdraw(idOfNotRegisteredAsset)

        then:
            assetWithdrawnEventWasNotPublishedFor(idOfNotRegisteredAsset)
    }

    @Override
    AssetAvailabilityRepository repository() {
        return repository
    }

    @Override
    TimeProvider timeProvider() {
        return timeProvider
    }
}
