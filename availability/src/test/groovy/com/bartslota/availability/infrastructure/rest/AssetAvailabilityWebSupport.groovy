package com.bartslota.availability.infrastructure.rest

import com.bartslota.availability.IntegrationSpec
import groovy.transform.SelfType
import org.springframework.test.web.servlet.ResultActions

import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

/**
 * @author bslota on 26/03/2022
 */
@SelfType(IntegrationSpec)
trait AssetAvailabilityWebSupport {

    ResultActions sendRegistrationRequestFor(String assetId) {
        sendAsAdmin("""
                                {
                                    "type" : "REGISTER",
                                    "assetId" : "${assetId}"
                                }
                            """)
    }

    ResultActions sendWithdrawalRequestFor(String assetId) {
        sendAsAdmin("""
                                {
                                    "type" : "WITHDRAW",
                                    "assetId" : "${assetId}"
                                }
                            """)
    }

    ResultActions sendActivationRequestFor(String assetId) {
        sendAsAdmin("""
                                {
                                    "type" : "ACTIVATE",
                                    "assetId" : "${assetId}"
                                }
                            """)
    }

    ResultActions sendLockRequestFor(String assetId) {
        send("""
                                {
                                    "type" : "LOCK",
                                    "assetId" : "${assetId}",
                                    "durationInMinutes" : "${someValidDuration().toMinutes()}"
                                }
                            """)
    }

    ResultActions sendIndefiniteLockRequestFor(String assetId) {
        send("""
                                {
                                    "type" : "LOCK_INDEFINITELY",
                                    "assetId" : "${assetId}"
                                }
                            """)
    }

    ResultActions sendUnlockRequestFor(String assetId) {
        send("""
                                {
                                    "type" : "UNLOCK",
                                    "assetId" : "${assetId}"
                                }
                            """)
    }

    private ResultActions send(String commandBody) {
        send(commandBody, "ADAM_123", "pass_123")
    }

    private ResultActions sendAsAdmin(String commandBody) {
        send(commandBody, "ADMIN_123", "admin_pass_123")
    }

    private ResultActions send(String commandBody, String username, String password) {
        mockMvc.perform(
                post("/api/assets/commands")
                        .with(httpBasic(username, password))
                        .contentType(APPLICATION_JSON)
                        .content(commandBody))
    }
}
