package com.bartslota.availability.infrastructure.events

import com.bartslota.availability.IntegrationSpec
import com.bartslota.availability.application.AssetAvailabilityStoreSupport
import com.bartslota.availability.application.AvailabilityService
import com.bartslota.availability.application.TimeSupport
import com.bartslota.availability.auth.AuthSupport
import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.domain.TimeProvider
import org.springframework.beans.factory.annotation.Autowired

import java.time.Duration

import static com.bartslota.availability.domain.AssetIdFixture.someAssetId
import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId

/**
 * @author bslota on 30/11/2021
 */
class AssetAvailabilityEventsIT extends IntegrationSpec implements AssetAvailabilityStoreSupport,
        AuthSupport, AssetAvailabilityEventsKafkaSupport, TimeSupport {

    @Autowired
    private AssetAvailabilityRepository assetAvailabilityRepository

    @Autowired
    private TimeProvider timeProvider

    @Autowired
    private AvailabilityService availabilityService

    def setup() {
        kafkaEventsTestListener.cleanup()
    }

    def "should emit AssetRegistered event when successfully registered asset"() {
        given:
            AssetId assetId = someAssetId()

        and:
            authorizedAsAdmin()

        when:
            availabilityService.registerAssetWith(assetId)

        then:
            assetRegisteredEventWasPublishedFor(assetId)
    }

    def "should emit AssetWithdrawn event when asset was successfully withdrawn"() {
        given:
            AssetAvailability asset = existingAsset()

        and:
            authorizedAsAdmin()

        when:
            availabilityService.withdraw(asset.id())

        then:
            assetWithdrawnEventWasPublishedFor(asset.id())
    }

    def "should emit AssetActivated event when asset was successfully activated"() {
        given:
            AssetAvailability asset = existingAsset()

        and:
            authorizedAsAdmin()

        when:
            availabilityService.activate(asset.id())

        then:
            assetActivatedEventWasPublishedFor(asset.id())
    }

    def "should emit AssetLocked event when asset was successfully locked"() {
        given:
            AssetAvailability asset = activatedAsset()
            OwnerId ownerId = someOwnerId()
            Duration duration = someValidDuration()

        and:
            authorizedAsCustomer()

        when:
            availabilityService.lock(asset.id(), ownerId, duration)

        then:
            assetLockedEventWasPublishedFor(asset.id(), ownerId, duration)
    }

    def "should emit AssetLockedIndefinitely event when asset was successfully locked indefinitely"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        and:
            authorizedAsCustomer()

        when:
            availabilityService.lockIndefinitely(asset.id(), ownerId)

        then:
            assetLockedIndefinitelyEventWasPublishedFor(asset.id(), ownerId)
    }

    def "should emit AssetUnlocked event when asset was successfully unlocked"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            authorizedAsCustomer()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        when:
            availabilityService.unlock(asset.id(), ownerId)

        then:
            assetUnlockedEventWasPublishedFor(asset.id(), ownerId)
    }

    @Override
    AssetAvailabilityRepository repository() {
        return assetAvailabilityRepository
    }

    @Override
    TimeProvider timeProvider() {
        timeProvider
    }
}
