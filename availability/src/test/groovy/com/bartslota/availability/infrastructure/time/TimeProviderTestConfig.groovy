package com.bartslota.availability.infrastructure.time

import com.bartslota.availability.domain.TimeProvider
import com.bartslota.availability.domain.TimeProviderFixture
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

import java.time.Instant

/**
 * @author bslota on 29/03/2022
 */
@TestConfiguration
class TimeProviderTestConfig {

    @Bean
    @Primary
    TimeProvider timeProvider() {
        TimeProviderFixture.timeProvider().thatIsFixedFor(Instant.now())
    }

}
