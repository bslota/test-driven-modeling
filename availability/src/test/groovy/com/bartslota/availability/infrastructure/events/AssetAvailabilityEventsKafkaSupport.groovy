package com.bartslota.availability.infrastructure.events

import com.bartslota.availability.application.TimeSupport
import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.events.AssetActivated
import com.bartslota.availability.events.AssetLockExpired
import com.bartslota.availability.events.AssetLocked
import com.bartslota.availability.events.AssetLockedIndefinitely
import com.bartslota.availability.events.AssetRegistered
import com.bartslota.availability.events.AssetUnlocked
import com.bartslota.availability.events.AssetWithdrawn
import com.bartslota.availability.events.DomainEvent
import com.bartslota.availability.infrastructure.kafka.KafkaEventsTestListener
import groovy.transform.SelfType
import org.awaitility.Awaitility
import org.springframework.beans.factory.annotation.Autowired

import java.time.Duration

import static com.bartslota.availability.PredefinedPollingConditions.SHORT_WAIT
import static java.util.concurrent.TimeUnit.SECONDS

/**
 * @author bslota on 09/01/2022
 */
@SelfType(TimeSupport)
trait AssetAvailabilityEventsKafkaSupport {

    @Autowired
    KafkaEventsTestListener kafkaEventsTestListener

    void assetRegisteredEventWasPublishedFor(AssetId assetId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetRegistered.from(assetId.asString(), timeProvider().instant()) }
        }
    }

    void assetWithdrawnEventWasPublishedFor(AssetId assetId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetWithdrawn.from(assetId.asString(), timeProvider().instant()) }
        }
    }

    void assetActivatedEventWasPublishedFor(AssetId assetId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetActivated.from(assetId.asString(), timeProvider().instant()) }
        }
    }

    void assetLockedIndefinitelyEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetLockedIndefinitely.from(assetId.asString(), ownerId.asString(), timeProvider().instant()) }
        }
    }

    void assetUnlockedEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetUnlocked.from(assetId.asString(), ownerId.asString(), timeProvider().instant()) }
        }
    }

    void assetLockExpiredEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetLockExpired.from(assetId.asString(), timeProvider().instant()) }
        }
    }

    void assetLockedEventWasPublishedFor(AssetId assetId, OwnerId ownerId, Duration duration) {
        Awaitility
                .await()
                .atMost(3, SECONDS)
                .until(() ->
                        kafkaEventsTestListener.containsEventFulfilling { DomainEvent e -> e == AssetLocked.from(assetId.asString(), ownerId.asString(), timeProvider().instant() + duration, timeProvider().instant()) }
                )
    }
}
