package com.bartslota.availability.infrastructure.rest

import com.bartslota.availability.IntegrationSpec
import io.restassured.response.ValidatableResponse
import io.restassured.specification.RequestSpecification
import spock.lang.Unroll

import static com.bartslota.availability.RandomUtils.random
import static com.bartslota.availability.domain.AssetIdFixture.someAssetIdValue
import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static io.restassured.RestAssured.given
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE

/**
 * @author bslota on 03/01/2022
 */
class AssetAvailabilitySecurityIT extends IntegrationSpec {

    @Unroll
    def "should forbid to #operationType asset with no authentication"() {
        given:
            RequestSpecification request = given()
                    .port(port)
                    .auth().none()
                    .contentType(APPLICATION_JSON_VALUE)
                    .body(commandBody)

        when:
            ValidatableResponse response =
                    request
                            .post("/api/assets/commands")
                            .then()
                            .log().everything()

        then:
            response.statusCode(401)

        where:
            operationType       | commandBody
            "register"          | """{ "type" : "REGISTER", "assetId" : "${someAssetIdValue()}" }"""
            "withdraw"          | """{ "type" : "WITHDRAW", "assetId" : "${someAssetIdValue()}" }"""
            "activate"          | """{ "type" : "ACTIVATE", "assetId" : "${someAssetIdValue()}" }"""
            "lock"              | """{ "type" : "LOCK", "assetId" : "${someAssetIdValue()}", "durationInMinutes" : "${someValidDuration().toMinutes()}" }"""
            "lock indefinitely" | """{ "type" : "LOCK_INDEFINITELY", "assetId" : "${someAssetIdValue()}" }"""
            "unlock"            | """{ "type" : "UNLOCK", "assetId" : "${someAssetIdValue()}" }"""
    }

    @Unroll
    def "should forbid to #operationType asset when credentials do not match"() {
        given:
            RequestSpecification request = given()
                    .port(port)
                    .auth().basic(random("username"), random("password"))
                    .contentType(APPLICATION_JSON_VALUE)
                    .body(commandBody)

        when:
            ValidatableResponse response =
                    request
                            .post("/api/assets/commands")
                            .then()
                            .log().everything()

        then:
            response.statusCode(401)

        where:
            operationType       | commandBody
            "register"          | """{ "type" : "REGISTER", "assetId" : "${someAssetIdValue()}" }"""
            "withdraw"          | """{ "type" : "WITHDRAW", "assetId" : "${someAssetIdValue()}" }"""
            "activate"          | """{ "type" : "ACTIVATE", "assetId" : "${someAssetIdValue()}" }"""
            "lock"              | """{ "type" : "LOCK", "assetId" : "${someAssetIdValue()}", "durationInMinutes" : "${someValidDuration().toMinutes()}" }"""
            "lock indefinitely" | """{ "type" : "LOCK_INDEFINITELY", "assetId" : "${someAssetIdValue()}" }"""
            "unlock"            | """{ "type" : "UNLOCK", "assetId" : "${someAssetIdValue()}" }"""
    }

    @Unroll
    def "should forbid to #operationType asset when authenticated user does not have an ADMIN role"() {
        given:
            RequestSpecification request = given()
                    .port(port)
                    .auth().basic("ADAM_123", "pass_123")
                    .contentType(APPLICATION_JSON_VALUE)
                    .body("""
                                {
                                    "type" : "REGISTER",
                                    "assetId" : "${someAssetIdValue()}"
                                }
                            """)

        when:
            ValidatableResponse response =
                    request
                            .post("/api/assets/commands")
                            .then()
                            .log().everything()

        then:
            response.statusCode(403)

        where:
            operationType       | username    | password         | commandBody
            "register"          | "ADAM_123"  | "pass_123"       | """{ "type" : "REGISTER", "assetId" : "${someAssetIdValue()}" }"""
            "withdraw"          | "ADAM_123"  | "pass_123"       | """{ "type" : "WITHDRAW", "assetId" : "${someAssetIdValue()}" }"""
            "activate"          | "ADAM_123"  | "pass_123"       | """{ "type" : "ACTIVATE", "assetId" : "${someAssetIdValue()}" }"""
            "lock"              | "ADMIN_123" | "admin_pass_123" | """{ "type" : "LOCK", "assetId" : "${someAssetIdValue()}", "durationInMinutes" : "${someValidDuration().toMinutes()}" }"""
            "lock indefinitely" | "ADMIN_123" | "admin_pass_123" | """{ "type" : "LOCK_INDEFINITELY", "assetId" : "${someAssetIdValue()}" }"""
            "unlock"            | "ADMIN_123" | "admin_pass_123" | """{ "type" : "UNLOCK", "assetId" : "${someAssetIdValue()}" }"""
    }
}
