package com.bartslota.availability.infrastructure.rest

import com.bartslota.availability.IntegrationSpec
import com.bartslota.availability.application.AssetAvailabilityStoreSupport
import com.bartslota.availability.application.TimeSupport
import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.domain.TimeProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.ResultActions

import static com.bartslota.availability.domain.AssetIdFixture.someAssetIdValue
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * @author bslota on 30/11/2021
 */
class AssetAvailabilityControllerIT extends IntegrationSpec implements AssetAvailabilityStoreSupport, AssetAvailabilityWebSupport, TimeSupport {

    @Autowired
    private AssetAvailabilityRepository assetAvailabilityRepository

    @Autowired
    private TimeProvider timeProvider

    def "should accept the registration of new asset"() {
        given:
            String assetId = someAssetIdValue()

        when:
            ResultActions result = sendRegistrationRequestFor(assetId)

        then:
            result.andExpect(status().isAccepted())
    }

    def "should reject the registration of already existing asset"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            ResultActions result = sendRegistrationRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should accept the withdrawal of existing asset"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            ResultActions result = sendWithdrawalRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isAccepted())
    }

    def "should reject the withdrawal of locked asset"() {
        given:
            AssetAvailability asset = lockedAsset()

        when:
            ResultActions result = sendWithdrawalRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should accept the activation of registered asset"() {
        given:
            AssetAvailability asset = existingAsset()

        when:
            ResultActions result = sendActivationRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isAccepted())
    }

    def "should reject the activation of not existing asset"() {
        given:
            String idOfNotExistingAsset = someAssetIdValue()

        when:
            ResultActions result = sendActivationRequestFor(idOfNotExistingAsset)

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should reject the activation of already activated asset"() {
        given:
            AssetAvailability asset = activatedAsset()

        when:
            ResultActions result = sendActivationRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should accept the locking of an activated asset"() {
        given:
            AssetAvailability asset = activatedAsset()

        when:
            ResultActions result = sendLockRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isAccepted())
    }

    def "should reject the locking of locked asset"() {
        given:
            AssetAvailability asset = lockedAsset()

        when:
            ResultActions result = sendLockRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should accept the indefinite locking of already locked asset"() {
        given:
            AssetAvailability asset = assetLockedBy(OwnerId.of("ADAM_123"))

        when:
            ResultActions result = sendIndefiniteLockRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isAccepted())
    }

    def "should reject the indefinite locking of asset locked by someone else"() {
        given:
            AssetAvailability asset = lockedAsset()

        when:
            ResultActions result = sendIndefiniteLockRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should accept the unlocking of already locked asset"() {
        given:
            AssetAvailability asset = activatedAsset()

        and:
            sendLockRequestFor(asset.id().asString())

        when:
            ResultActions result = sendUnlockRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isAccepted())
    }

    def "should reject the unlocking of asset locked by someone else"() {
        given:
            AssetAvailability asset = lockedAsset()

        when:
            ResultActions result = sendUnlockRequestFor(asset.id().asString())

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    @Override
    AssetAvailabilityRepository repository() {
        return assetAvailabilityRepository
    }

    @Override
    TimeProvider timeProvider() {
        return timeProvider
    }
}
